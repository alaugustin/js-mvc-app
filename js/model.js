var app = {}

jQuery(function($){
	app.header();
	app.body();
	app.footer();
	
	globalContent();
	todaysDate();
	buildNavLinks();
	buildForm();
	buildCopyrightDate();
});

function globalContent() {
	var gcObject = {
		"mainHeading" : "Test Heading",
		"footerContent" : "Site name here",
		"foo" : "bar",
	};
	
	$("#mainTitle").append(gcObject.mainHeading);
	$("footer").append(gcObject.footerContent);
}

function todaysDate() {
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();

	$("#dateHolder").append(month + "/" + day + "/" + year);
}

function buildNavLinks() {
	var navLinks = new Array();
	navLinks[0] = "Home";
	navLinks[1] = "About";
	navLinks[2] = "Contact";

	ul = $('<ul id="mainNav">');
	for ( i = 0; i < navLinks.length; i++) {
		ul.append('<a href="javascript:void(0);"><li>' + navLinks[i] + '</li></a>');
	}

	$("nav").append(ul);
}

function buildForm() {
	function initFormStore() {
		if (localStorage["fname"]) {
			$('#fname').val(localStorage["fname"]);
		}
		if (localStorage["lname"]) {
			$('#lname').val(localStorage["lfname"]);
		}
		if (localStorage["email"]) {
			$('#email').val(localStorage["email"]);
		}
	}

	initFormStore();

	$('.stored').keyup(function() {
		localStorage[$(this).attr('name')] = $(this).val();
	});
	
	$('#localStorageTest').submit(function() {
		localStorage.clear();
		return false
	});

	formDataActions();

function formDataActions(){
	var fname = localStorage.getItem('fname');

	if (fname == null){
		$("#userName").append("Hello guest");
	}else{
		$("#userName").append("Hello&nbsp;" + fname + "&nbsp;");
	}

	$('#content').append('<div id="mailtoLink"></div>');

	var email = localStorage.getItem('email');
	if (email == null){
		$("#mailtoLink").css('display', 'none');
	}else{
		$("#mailtoLink").append('<a href="mailto:' + email + '?Subject=Subject%20here">Email your data</a>');
		}
	}
}
function buildCopyrightDate() {
	var year = new Date().getFullYear();

	if (year > 2012) {
		$("footer").append('&#169; 2012&nbsp;-&nbsp;' + year + '&nbsp;');
	} else {
		$("footer").append('&#169;&nbsp;' + year + '&nbsp;');
	}
}