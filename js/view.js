app.header = function(){
	$("body").append(
		'<header></header>' +
		'<nav></nav>');
	
	$("header").append(
		'<h1 id="mainTitle"></h1>' +
		'<span id="userName"></span>' +
		'<span id="dateHolder"></span>');
}
app.body = function(){
	$("body").append('<div id="content"></div>');
	$('#content').append(
		'<form method="GET">' +
			'<fieldset>' +
				'<legend>Your details</legend>' +
				'<ol>' +
					'<li>' +
						'<label for=name>First Name</label>' +
						'<input type="text" name="fname" id="fname" class="stored" value="" required autofocus />' +
						'<label for=name>Last Name</label>' +
						'<input type="text" name="lname" id="lname" class="stored" value="" />' +
					'</li>' +
					'<li>' +
						'<label for=email>Email</label>' +
						'<input type="email" name="email" id="email" class="stored" value="" />' +
					'</li>' +
				'</ol>' +
			'</fieldset>' +
			'<fieldset>' +
				'<button id="submitBtn" type=submit>&nbsp;Submit&nbsp;</button>' +
				'<button id="clearBtn" type=submit>&nbsp;Clear&nbsp;</button>' +
			'</fieldset>' +
		'</form>'
	);
}
app.footer = function(){
	$("body").append('<footer></footer>');
}
